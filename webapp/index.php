<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Garden Buzz</title>
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script type="text/javascript">
		// Avoid issues in IE* where console is not defined!
		if (!window.console) {
			window.console = {};
			window.console.log = function () {
			};
		}

		console.log("Host is " + location.host);
		if (location.host.search('localhost') >= 0 || location.host.search("bondinorthpro") >= 0) {
			console.log("Setting up for localhost.");
			gbBasePath = location.protocol + '//' + location.host + "/gardenbuzz/";
		} else {
			console.log("Setting up for Default.");
			gbBasePath = location.protocol + '//' + location.host + "/gardenbuzz/";
		}
		mxImageBasePath = 'img';
	</script>
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35101738-1']);
		_gaq.push(['_trackPageview']);

		(function () {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();

	</script>
</head>
<body>

<div id="wrapper">
	<div id="banner">
		<h1>GardenBuzz</h1>

		<h2>A division of Sharper Tool Gardening, LLC.</h2>
	</div>
	<div id="projectBar" class="shadow">

		<div id="logo_header">
			<img src="img/gardenbuzz_logo_flowerpotbee.png"
				 height="220px" alt="Gardenbuzz"/>
		</div>

		<div class="projectbar_button" id="home_button">
			Home
		</div>

		<div class="projectbar_button" id="help_button">
			Help
		</div>

		<div class="projectbar_button" id="veggie_button">
			Getting Started
		</div>

		<div class="projectbar_button" id="project_button">
			<img src="img/gardenbuzz_logo_veggies.png"
				 width="120px" id="arrow"/>
			Featured Gardener
		</div>
	</div>

	<div id="sitenav">
		<p>Site Navigation</p>
		<ul>
			<li id="tab_main" class="tab_main tab_first tab_hot">Home</li>
			<li id="tab_text" class="tab_text" style="display:none;">Garden Coach</li>
			<li id="tab_report" class="tab_report">Plant Tips</li>
			<li id="tab_chart_1" class="tab_charts">My First Garden</li>
			<li id="tab_chart_2" class="tab_charts">Plant Database</li>
		</ul>
	</div>

	<div id="main">
		<h2>GardenBuzz Happenings</h2>

		<p>Garden Buzz is currently being developed. We plan to offer an interesting and informative site for
			Idaho Treasure Valley gardeners. Garden Buzz is owned and operated by Vicki Henderson. Vicki is
			an Advanced Master Gardener from the University of Idaho Extension. She has worked as a professional
			gardener
			for some of the nicest gardens in Boise. Now, she brings her insight and experience to everyone, through
			her website.

		<p>

		<p>
			Vicki is also a member of the <a href="http://www.idabees.org/">Treasure Valley Beekeepers Club</a>. She
			keeps bees at
			her home in Southwest Idaho where they provide an abundant source of pollinators for the plants and trees in
			the neighborhood. They also supply a source of healthy, local honey for her family and friends.
		</p>

		<h2>What we have planned</h2>

		<h3>Featured Gardener</h3>

		<p>Our featured gardener segment will include video and an interview of local gardeners from across the
			treasure valley. Real gardeners, big and small, elaborate and simple. See your favorite plants in a variety
			of garden settings. Find out what tips and tricks other gardeners use. Or, just enjoy some of the prettiest
			gardens
			that Idaho has to offer.</p>

		<h3>Plant Information</h3>

		<p>We are working on a plant data base that will list plants that you can grow in Idaho. Expect a few surprises <span>
                I didn't know you could grow those here?!</span>. </p>

		<p>Find information about the area specific zones that plants
			thrive in, and tips on what you can do to grow a particular plant in zone. While the entire treasure valley
			resides
			in a single official zone, there are many micro-zones throughout the valley, each with their own unique soil,
			water
			and weather patterns.
		</p>

		<h3>Treasure Valley Nurseries</h3>

		<p>There are many nurseries around the Treasure Valley, each offering a unique mix of garden items, plans,
			flowers, shrubs, and more. Find out about each of nurseries, where they are, photos, and information about
			the types of plants they carry, as well as unique
			services offered.
		</p>

		<h3>Beginner Gardener How-To Tips and Videos</h3>

		<p>
			If you are just starting out at gardening, or want to learn a few tricks, tune in to <span>GardenBuzz</span>
			for information about the latest techniques in Treasure Valley Gardening.
		</p>
	</div>

	<div id="announcement">
		<h2>The Latest Buzz</h2>

		<ul class="last">
			<li>
				For information about Sharper Tool, visit <a href="http://sharpertoolgardening.com">our business
				website.</a>
			</li>
			<li>
				See what Vicki is currently working on at her blog site <a href="http://vickisgardentips.com">Vicki's
				Garden Tips</a>.
			</li>
		</ul>
	</div>

	<div id="copyright">
		<p>Copyright 2012, Sharper Tool Gardening, LLC.</p>
	</div>

</div>

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.corner.js"></script>
<script type="text/javascript" src="js/jquery.layout.min-1.2.0.js"></script>
<script type="text/javascript" src="js/underscore.js"></script>

<!-- <script type="text/javascript" src="js/flot/jquery.flot.js"></script> -->
<script type="text/javascript" src="js/splitter.js"></script>
<script type="text/javascript" src="js/jquery.download.js"></script>
<script type="text/javascript" src="js/jquery.imgpreload.min.js"></script>

<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.editable.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/log4javascript_uncompressed.js"></script>


<script type="text/javascript" src="js/gbClient.js"></script>
<script type="text/javascript" src="js/gbLoad.js"></script>
<script type="text/javascript" src="js/gardenbuzz.js"></script>
</body>
</html>
