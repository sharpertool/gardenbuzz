/**
 * Created with IntelliJ IDEA.
 * User: kutenai
 * Date: 8/13/12
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */

var gui = null;
var log = null;

$(document).ready(function() {

    log = log4javascript.getLogger();

    var popUpAppender = new log4javascript.PopUpAppender();
    var popUpLayout = new log4javascript.PatternLayout("%d{HH:mm:ss} %-5p - %m%n");
    popUpAppender.setLayout(popUpLayout);
    //log.addAppender(popUpAppender);

    log.info("Initializing GardenBuzz on " + location.host);

    gui = new gbGui();

    gui.init();

});
